#pragma once

#include "ui.hpp"
#include "tab.hpp"
#include "lang/text.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <imgui.h>
#include <tuple>

/// Vordeklaration.
class Buch;

/**
 * Hauptansicht.
 *
 * + Menü links = `show_config()` und `ui_verswahl()` und `ui_uebersetzungswahl()`
 * + Menübalken oben = `show_lesezeichen()` und `show_suche()` und `show_einstellungen()` und `show_karte()`
 * + Lesetexte = `show_texte()`
 *
 * @note Mainmenu::show() startet eine Schleife bis das Fenster geschlossen wird.
 */
class Mainmenu final {

    friend class Suche;

    /// Horizontale Aufteilung
    static constexpr float FAKTOR_PART1 = 0.15f;

    /// Standardabstand z.B. oberer Rand
    static constexpr float PADDING = 16.f;

    /// ID Lesezeichenfenster
    static inline const char* id_lesezeichen = TEXT_LESEZEICHEN "##win_lesezeichen";

    /// ID Suchfenster
    static inline const char* id_suche = TEXT_SUCHE "##win_suchen";

    /// ID Einstellungenfenster
    static inline const char* id_einstellungen = TEXT_EINSTELLUNGEN "##win_einstellungen";

    /// ID Karte
    static inline const char* id_karte = TEXT_KARTE "##win_karte";

public:

    /// Ctor.
    Mainmenu();

    /// Standard-Ctor über SF-Fenster.
    explicit Mainmenu(sf::RenderWindow& window);

    /// Destruktor.
    ~Mainmenu();

    /// Startet die Fensteranzeige (Schleife).
    void show();

private: // UI

    void show_config();

    void show_texte();

    void show_lesezeichen();

    void show_suche();

    void show_einstellungen();

    void show_karte();

    void show_bibelmanager();

    void ui_verswahl();

    void ui_uebersetzungswahl();

    void farben_setzen();

    /// Scrollt die Textansicht wieder an den Anfang
    void scroll_anfang();

    Tab& get_tab() { return tabs[tab]; }

    sf::RenderWindow* window = nullptr;

    /// Loop?
    bool open = true;

    bool open_lesezeichen = false;

    bool open_suche = false;

    bool open_einstellungen = false;

    bool open_karte = false;

    bool open_bibelmanager = false;

    bool reset_scroll = false;

private: // Arbeitsdaten

    /// Anzuzeigende Übersetzungen [Sprache] -> Set von [ID].
    std::vector<std::tuple<std::string, std::string>> keys;

    /// Tabs
    std::vector<Tab> tabs;

    /// Anzuzeigender Tab.
    unsigned tab = 0;

private: // Stil

    /// Größe der Schrift (1-6).
    unsigned text_groesse = 3;

    /// Textfarbe.
    ImVec4 farbe_text = ImGui::GetStyleColorVec4(ImGuiCol_Text);

    /// Fensterhintergrundfarbe.
    ImVec4 farbe_hg = ImGui::GetStyleColorVec4(ImGuiCol_WindowBg);

    /// Farbe der Versziffern.
    ImVec4 farbe_versziffern = ImColor(UI::FARBE1);

};
