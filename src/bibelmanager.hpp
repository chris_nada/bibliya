#pragma once

#include "lang/text.hpp"

class Bibelmanager {

public:

    static constexpr const char* POPUP_ID = TEXT_BM_FENSTERTITEL "##bibelmanager";

    void show_ui();

private:

    void pruef_downloads();

};
