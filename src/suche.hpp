#pragma once

#include "tab.hpp"

class Mainmenu;

class Suche {

public:

    static void show(const char* id_suche, bool* open_suche, Mainmenu* mainmenu);

};
