#include "suche.hpp"
#include "ui.hpp"
#include "lesezeichen.hpp"
#include "uebersetzung.hpp"
#include "mainmenu.hpp"
#include "helfer/sonstiges.hpp"
#include <imgui.h>

void Suche::show(const char* id_suche, bool* open_suche, Mainmenu* mainmenu) {
    UI::push_font();
    ImGui::SetNextWindowSizeConstraints({400, 200}, {2000,2000});
    if (ImGui::Begin(id_suche, open_suche)) {
        static std::vector<Lesezeichen> ergebnisse;
        static auto scroll_alt = std::make_pair(false, ImGui::GetScrollY());
        if (scroll_alt.first) {
            scroll_alt.first = false;
            ImGui::SetScrollY(scroll_alt.second);
        }

        // X Größe sicherstellen
        if (ImGui::GetWindowSize().x < 580.f) ImGui::SetWindowSize({580.f, ImGui::GetWindowSize().y});

        // Neue Suche
        static char suchbegriff_c_str[0xFF] = "";
        static bool at  = true;
        static bool nt  = true;
        static bool etc = true;
        static std::string suchbegriff;
        auto suchen = [&]() {
            suchbegriff = std::string(suchbegriff_c_str);
            if (suchbegriff.empty()) return;
            ergebnisse = Uebersetzung::suche(suchbegriff_c_str, at, nt, etc);
        };
        ImGui::SetNextItemWidth(400);
        if (ImGui::InputTextWithHint("##input_suche", TEXT_SUCHBEGRIFF,
                                     suchbegriff_c_str, IM_ARRAYSIZE(suchbegriff_c_str),
                                     ImGuiInputTextFlags_EnterReturnsTrue))
        {
            suchen();
        }
        ImGui::SameLine();
        UI::push_icons();
        if (ImGui::Button("\uF002##btn_suche_start")) suchen();
        ImGui::PopFont();
        if (ImGui::IsItemHovered()) ImGui::SetTooltip(TEXT_TT_SUCHE_STARTEN);
        UI::tooltip(TEXT_TT_SUCHE_GROSS_KLEIN);
        
        // Filter
        ImGui::Checkbox(TEXT_ALTES_TESTAMENT, &at);
        if (ImGui::IsItemHovered()) ImGui::SetTooltip(TEXT_ALTES_TESTAMENT_DURCHSUCHEN);
        ImGui::SameLine();
        ImGui::Checkbox(TEXT_NEUES_TESTAMENT, &nt);
        if (ImGui::IsItemHovered()) ImGui::SetTooltip(TEXT_NEUES_TESTAMENT_DURCHSUCHEN);
        ImGui::SameLine();
        ImGui::Checkbox(TEXT_UNZUGEORDNET, &etc);
        if (ImGui::IsItemHovered()) ImGui::SetTooltip(TEXT_UNZUGEORDNET_DURCHSUCHEN);
        ImGui::NewLine();

        // Suchergebnisse auflisten
        if (!suchbegriff.empty()) {
            if (ergebnisse.empty()) ImGui::Text(TEXT_KEINE_ERGEBNISSE_FUER, suchbegriff.c_str());
            else {
                ImGui::Text(TEXT_ERGEBNISSE, ergebnisse.size());
                for (unsigned i = 0; i < ergebnisse.size(); ++i) {
                    const Lesezeichen& l = ergebnisse[i];
                    if (Buch::get_buecher().count(l.buch) == 0) continue;
                    const Buch& l_buch = Buch::get_buecher().at(l.buch);

                    // Auswählen
                    UI::push_icons();
                    if (std::string id("\uF061##suche_goto_" + std::to_string(i)); ImGui::Button(id.c_str())) {
                        scroll_alt = std::make_pair(true, ImGui::GetScrollY()); // Scrollpos merken, Workaround
                        Tab& aktueller_tab = mainmenu->get_tab();
                        aktueller_tab.set_buch(&l_buch);
                        aktueller_tab.auswahl_kapitel = l.kapitel;
                        aktueller_tab.auswahl_vers = l.vers;
                        ImGui::PopFont();
                        break;
                    }
                    else ImGui::PopFont();
                    if (ImGui::IsItemHovered()) ImGui::SetTooltip(TEXT_TT_IM_AKTUELLEN_TAB_ANZEIGEN);

                    // Auswählen - neuer Tab
                    UI::push_icons();
                    ImGui::SameLine();
                    if (std::string id("\uF196##suche_goto_new_tab" + std::to_string(i)); ImGui::Button(id.c_str())) {
                        scroll_alt = std::make_pair(true, ImGui::GetScrollY()); // Scrollpos merken, Workaround
                        Tab neuer_tab;
                        neuer_tab.set_buch(&l_buch);
                        neuer_tab.auswahl_kapitel = l.kapitel;
                        neuer_tab.auswahl_vers = l.vers;
                        mainmenu->tabs.push_back(neuer_tab);
                        ImGui::PopFont();
                        break;
                    }
                    else ImGui::PopFont();
                    if (ImGui::IsItemHovered()) ImGui::SetTooltip(TEXT_TT_IN_NEUEM_TAB_ANZEIGEN);

                    // Textpassage
                    ImGui::SameLine();
                    const bool u_good = l.uebersetzung !=nullptr;
                    ImGui::Text("%s %u:%u (%s)",
                            l_buch.get_name().c_str(), l.kapitel, l.vers,
                            u_good ? l.uebersetzung->get_name().c_str() : "?");
                    if (u_good) { // Textvorschau
                        std::string text_preview = l.uebersetzung->get_text(l_buch.get_osis_id(l.kapitel, l.vers));
                        Sonstiges::textwrap(text_preview, 40u);
                        if (ImGui::IsItemHovered()) {
                            ImGui::SetTooltip("%s",text_preview.c_str());
                        }
                    }
                }
            }
        }
    }
    ImGui::End();
    ImGui::PopFont();
}
