#pragma once

#include <cstdint>

namespace nada {

class Clock {

public:

    static uint64_t ticks();

    Clock();

    void reset();

    [[nodiscard]] uint64_t ms() const;

    [[nodiscard]] float s() const;

private:

    uint64_t time_start;

};}
