#include "netz.hpp"
#include <curl/curl.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include "clock.hpp"

Netz::Netz () : in_arbeit(false), fortschritt(0.f) {
    curl_global_init(CURL_GLOBAL_DEFAULT);
    curl = curl_easy_init ();
}

Netz::~Netz () {
    curl_easy_cleanup(curl);
    curl_global_cleanup();
}

struct Writebuffer {
    Writebuffer() : size(0) { memory = (char*)malloc(1); }
    ~Writebuffer() { free(memory); }
    char* memory;
    size_t size;
};
 
size_t write_to_buffer_callback(void* contents, size_t size, size_t nmemb, void* userp) {
    const size_t realsize = size * nmemb;
    Writebuffer* mem = (Writebuffer*)userp;
    char* ptr = (char*)realloc(mem->memory, mem->size + realsize + 1);
    mem->memory = ptr;
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
    return realsize;
}

int fortschritt_callback(void* dataptr, curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow) {
    thread_local nada::Clock clock;
    (void) ultotal; (void) ulnow;
    if (clock.ms() > 100) {
        clock.reset();
        const float temp = (float)dlnow / (float)dltotal;
        std::atomic<float>* f = (std::atomic<float>*)dataptr;
        f->store(temp);
    }
    return 0; // 0 = weiter herunterladen, 1 = abbrechen
}

void Netz::runterladen (const std::string& url) {
    if (in_arbeit) return;
    in_arbeit = true;
    this->url = url;
    this->future = std::async(std::launch::async, &Netz::arbeit, this);
}

std::string Netz::arbeit () {
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str ());
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

        curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, fortschritt_callback);
        curl_easy_setopt(curl, CURLOPT_XFERINFODATA, &fortschritt);
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);

        Writebuffer wb;
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_buffer_callback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)&wb);
        CURLcode res = curl_easy_perform(curl);
        ergebnis.assign(wb.memory, wb.size);
        std::cout << '\t' << wb.size << "bytes geladen von " << url << std::endl;

        // Fehler TODO
        if (res != CURLE_OK) {
            std::cerr << "Netz::arbeit() fehlgeschlagen: " << curl_easy_strerror(res) << '\n';
        } else fortschritt.store(1.f);
    }
    in_arbeit = false;
    return ""; // mit std::future arbeiten ist einfacher als mit std::thread
}
