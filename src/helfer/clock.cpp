#include "clock.hpp"
#include <chrono>

uint64_t nada::Clock::ticks() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

nada::Clock::Clock() :
    time_start(ticks())
{
    //
}

void nada::Clock::reset() {
    time_start = ticks();
}

uint64_t nada::Clock::ms() const {
    return ticks() - time_start;
}

float nada::Clock::s() const {
    return static_cast<float>(ms()) * 0.001f;
}
