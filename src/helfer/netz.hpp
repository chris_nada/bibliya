#pragma once

#include <string>
#include <atomic>
#include <future>

class Netz final {

public:

    Netz();

    ~Netz();

    /// Startet das Herunterladen und speichern gegebener url in 
    void runterladen(const std::string& url);

    /// Wenn true, darf nicht `get_ergebnis()` aufgerufen werden!
    [[nodiscard]] bool is_in_arbeit() const { return in_arbeit.load(); }

    /// Darf nur aufgerufen werden, wenn `is_in_arbeit() == false`!
    [[nodiscard]] const std::string& get_ergebnis() const { return ergebnis; }

    /// Download-Fortschritt im Bereich 0.0 - 1.0.
    [[nodiscard]] float get_fortschritt() const { return fortschritt.load(); }

private:

    /// Funktion für den Arbeits-Thread
    std::string arbeit();

private:

    std::string ergebnis;

    std::string url;

    std::atomic<bool> in_arbeit;

    std::atomic<float> fortschritt;

    std::future<std::string> future;

    void* curl;

};
