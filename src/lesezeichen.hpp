#pragma once

#include "tab.hpp"
#include <vector>

class Uebersetzung;

/// Repräsentiert ein einzelnes Lesezeichen, d.h. eine Referenz zu einer Textstelle.
class Lesezeichen final {

public:

    Lesezeichen() = default;

    Lesezeichen(const std::string& notiz, const std::string& buch, unsigned int kapitel, unsigned int vers);

    /// Rendert das Fenster.
    static void show(const char* id_lesezeichen, bool* open_lesezeichen, Tab& aktueller_tab);

    /// Lese-/Schreibzugriff auf alle Lesezeichen.
    static std::vector<Lesezeichen>& alle();

    static void add(const Lesezeichen& neu);

    static void remove(std::size_t pos);

    /// Optionale Notiz des Nutzers.
    std::string notiz;

    /// Lesezeichen: Key des Buches.
    std::string buch;

    /// Wird nur von der Suchfunktion verwendet, um die gefundene Textpassage einer Übersetzung zuzuordnen.
    const Uebersetzung* uebersetzung = nullptr;

    /// Lesezeichen: Kapitel.
    unsigned kapitel;

    /// Lesezeichen: Vers.
    unsigned vers;

    /// Vergleicht nach Position im Buch (vorne < hinten).
    bool operator<(const Lesezeichen& rhs) const;

    template<class Archiv>
    void serialize(Archiv& ar) {
        ar(CEREAL_NVP(notiz), CEREAL_NVP(buch), CEREAL_NVP(kapitel), CEREAL_NVP(vers));
    }

private:

    /// Speicher für alle Lesezeichen.
    static std::vector<Lesezeichen> lesezeichen;

    /// Speichert alle aktuellen Lesezeichen.
    static void save();

};
