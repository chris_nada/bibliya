#pragma once

#ifdef SPRACHE_DEUTSCH
#define SPRACHE
#include "lang_de.hpp"
#endif

#ifdef SPRACHE_UKRAINISCH
#define SPRACHE
#include "lang_ua.hpp"
#endif

#ifdef SPRACHE_ENGLISCH
#define SPRACHE
#include "lang_en.hpp"
#endif

#ifndef SPRACHE
#error "You need to specify for which language (SPRACHE) to compile"
#endif
