/**
 * Bibliya
 * English user interface
 * TODO
 */
#define BUECHER_INI "buecher_en.ini"
#define TEXT_TEXTE_WERDEN_GELADEN "Loading texts..."
#define TEXT_LESEZEICHEN "Bookmarks"
#define TEXT_NOTIZ "Notes"
#define TEXT_TT_LESEZEICHEN_HINZUFUEGEN "Add bookmark"
#define TEXT_TT_LESEZEICHEN_ENTFERNEN "Remove bookmark"
#define TEXT_SUCHE "Search"
#define TEXT_EINSTELLUNGEN "Preferences"
#define TEXT_KARTE "Map"
#define TEXT_BIBELMANAGER "Manage translations"

#define TEXT_TT_UEBERSETZUNG_ENTFERNEN "Remove translation from View"
#define TEXT_TT_UEBERSETZUNG_NACH_LINKS "Move translation left"
#define TEXT_TT_UEBERSETZUNG_NACH_RECHTS "Move translation right"
#define TEXT_TT_UEBERSETZUNG_HINZUFUEGEN "Add Translation"

#define TEXT_ANZEIGEN "View"
#define TEXT_GANZES_KAPITEL_KOPIEREN "Copy chapter"
#define TEXT_VERSTEXT "Text"
#define TEXT_KOPIEREN "copy"

#define TEXT_SPRACHE "Language"
#define TEXT_UEBERSETZUNG "Translation"

#define TEXT_BUCH "Book"
#define TEXT_KAPITEL "Chapter"
#define TEXT_VERS "Verse"
#define TEXT_DARSTELLUNG "View"
#define TEXT_1_VERS "1 Verse"
#define TEXT_5_VERSE "5 Verses"

#define TEXT_ALTES_TESTAMENT "Old Testament"
#define TEXT_NEUES_TESTAMENT "New Testament"
#define TEXT_UNZUGEORDNET "Miscellaneous"

#define TEXT_VERSGROESSE "Verse text size"
#define TEXT_TT_RESET "Reset"
#define TEXT_TEXTFARBE "Text colour"
#define TEXT_HINTERGRUNDFARBE "Background color"

#define TEXT_SUCHBEGRIFF "Search term"
#define TEXT_TT_SUCHE_STARTEN "Start search"
#define TEXT_TT_SUCHE_GROSS_KLEIN "Text search is case sensitive."
#define TEXT_NUR_NT "New Testament only"
#define TEXT_TT_NUR_NT "Only search new testament"
#define TEXT_KEINE_ERGEBNISSE_FUER "No results for %s"
#define TEXT_TT_IM_AKTUELLEN_TAB_ANZEIGEN "Show in current tab"
#define TEXT_TT_IN_NEUEM_TAB_ANZEIGEN "Show in new tab"

#define TEXT_BM_FENSTERTITEL "Manage translations"
#define TEXT_BM_INSTALLIERT "Installed"
#define TEXT_BM_SPRACHE "Language"
#define TEXT_BM_JAHR "Year"
#define TEXT_BM_TITEL "Title"
#define TEXT_BM_VERWALTEN "Manage"
#define TEXT_BM_TT_INSALLIEREN "Download and install"
#define TEXT_BM_TT_DEINSTALLIEREN "Remove"
#define TEXT_BM_DOWNLOADS "Downloads"
#define TEXT_BM_TT_AENDERUNGEN_INFO "New or removed translations are only visible after program restart."
#define TEXT_BM_TT_DOWNLOAD_ERFOLG "Download complete."
#define TEXT_BM_TT_DOWNLOAD_SERVER_NICHT_ERREICHBAR "Text server currently unavailable."
#define TEXT_BM_TT_NAECHSTE_SEITE "Next page"
#define TEXT_BM_TT_VORIGE_SEITE "Previous page"
