#include "bibelmanager.hpp"
#include "ui.hpp"
#include "helfer/sonstiges.hpp"
#include "helfer/netz.hpp"
#include "lang/text.hpp"
#include <imgui.h>
#include <misc/cpp/imgui_stdlib.h>
#include <string>
#include <utility>
#include <vector>
#include <functional>
#include <fstream>
#include <iostream>
#include <filesystem>

/// Herunterladares Paket
struct Paket {
    enum class Status { NONE, DOWNLOADING, DOWNLOAD_SUCCESS, DOWNLOAD_FAILURE };
    std::string sprache; // de, csl, lat, ru, ua...
    std::string dateipfad; // relativer pfad zum ablegen der Datei
    std::string titel; // Name / Titel
    std::string beschreibung; // Inhalt des description Tags
    unsigned jahr; // Erscheinungsjahr. 0 = unbekannt.
    std::string url; // Download-URL
    bool installiert;
    Status status = Status::NONE;
    std::unique_ptr<Netz> download; // Download: Start + Daten
    void check_installiert () { std::ifstream in(dateipfad); installiert = in.good(); }
};

std::vector<Paket> refresh_data() {
    std::vector<Paket> v;
    if (std::ifstream csv("data/pakete.dat"); csv.good()) {
        for (std::string zeile; std::getline(csv, zeile);) {
            if (zeile.empty() || zeile[0] == '#') continue; // Kommentarzeile
            std::vector<std::string> tokens = Sonstiges::tokenize(zeile, '$');
            if (tokens.size() != 6) {
                std::cerr << "[Warnung] Zeile ungueltig in data/pakete.dat: " << zeile << '\n';
                continue;
            }
            v.emplace_back();
            Paket& b = v.back();
            b.sprache = tokens[0];
            b.dateipfad = tokens[1];
            b.jahr = std::atoi(tokens[2].c_str());
            b.titel = tokens[3];
            b.url = tokens[4];
            b.beschreibung = tokens[5];
            b.check_installiert();
        }
    } else std::cerr << "[Fehler] Nicht gefunden: data/pakete.dat\n";
    return v;
}

/// Paketdaten
std::vector<Paket> pakete = refresh_data();
std::vector<Paket*> pakete_gefiltert;

void Bibelmanager::show_ui() {
    pruef_downloads();

    ImGui::SetNextWindowSizeConstraints({400, 200}, {2000,2000});
    UI::push_font(3);
    if (!ImGui::BeginPopupModal(Bibelmanager::POPUP_ID, nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
        ImGui::PopFont();
        return;
    }
    UI::push_icons();
    if (ImGui::Button("\uF00D##bibelmanager_close")) ImGui::CloseCurrentPopup();

    // Tabellen Pagination
    static unsigned seite = 0;
    static const unsigned p_pro_seite = 20;
    const unsigned seiten = pakete_gefiltert.size() / p_pro_seite;
    if (ImGui::SameLine(); ImGui::Button("\uF053") && seite > 0) seite--; // <
    ImGui::PopFont(); UI::push_font();
    if (ImGui::IsItemHovered()) ImGui::SetTooltip(TEXT_BM_TT_VORIGE_SEITE);
    ImGui::PopFont(); UI::push_icons();
    if (ImGui::SameLine(); ImGui::Button("\uF054") && seite < seiten) seite++; // >
    ImGui::PopFont(); UI::push_font();
    if (ImGui::IsItemHovered()) ImGui::SetTooltip(TEXT_BM_TT_NAECHSTE_SEITE);
    ImGui::PopFont();
    ImGui::SameLine();
    ImGui::Text("%d/%d", seite + 1, seiten + 1);

    ///UI::push_font(3);

    // Suche
    static std::string suche;
    ImGui::SameLine();
    std::string suche_alt(suche);
    ImGui::InputTextWithHint("##bm_suche", TEXT_SUCHBEGRIFF, &suche);
    if (suche != suche_alt) seite = 0;

    // Aktuelle Downloads
    bool download_vorhanden = false;
    for (auto& p : pakete) if (p.status != Paket::Status::NONE) {
        if (!download_vorhanden) {
            download_vorhanden = true;
            ImGui::TextUnformatted(TEXT_BM_DOWNLOADS);
        }
        ImGui::ProgressBar(p.download ? p.download->get_fortschritt() :
                            p.status == Paket::Status::DOWNLOAD_SUCCESS ? 1.0f : 0.0f, {100,0});
        ImGui::SameLine();
        ImGui::TextUnformatted(p.titel.c_str());
        if (p.status == Paket::Status::DOWNLOAD_SUCCESS || p.status == Paket::Status::DOWNLOAD_FAILURE) {
            ImGui::SameLine();
            UI::push_icons();
            if (p.status == Paket::Status::DOWNLOAD_SUCCESS) {
                ImGui::TextUnformatted("\uF00C"); // Erfolg
                ImGui::PopFont();
                UI::tooltip(TEXT_BM_TT_DOWNLOAD_ERFOLG "\n" TEXT_BM_TT_AENDERUNGEN_INFO);
            } else {
                ImGui::TextUnformatted("\uF06A"); // Fehlgeschlagen
                ImGui::PopFont();
                UI::tooltip(TEXT_BM_TT_DOWNLOAD_SERVER_NICHT_ERREICHBAR);
            }
            const std::string btn_ok_id = "OK##ok" + p.dateipfad; // Aus fertig/fehlgeschlagen-Liste entfernen
            if (ImGui::SameLine(); ImGui::Button(btn_ok_id.c_str())) p.status = Paket::Status::NONE;
        }
    }

    // Tabelle zeichnen
    static const ImGuiTableFlags flags = ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg | ImGuiTableFlags_SizingFixedFit;
    if (ImGui::BeginTable("##bibelmanagertable", 5, flags)) {
        ImGui::TableNextRow();
        ImGui::TableNextColumn();
        // Tabellen-Header mit Sortierfunktionen
        if (ImGui::Selectable(TEXT_BM_INSTALLIERT, false, ImGuiSelectableFlags_DontClosePopups)) {
            static bool richtung = false;
            richtung = !richtung;
            std::sort(pakete.begin(), pakete.end(), [](const Paket& l, const Paket& r) { 
                if (richtung) return l.installiert < r.installiert; 
                else return l.installiert > r.installiert; 
            });
        }
        ImGui::TableNextColumn();
        if (ImGui::Selectable(TEXT_BM_SPRACHE, false, ImGuiSelectableFlags_DontClosePopups)) {
            static bool richtung = false;
            richtung = !richtung;
            std::sort(pakete.begin(), pakete.end(), [](const Paket& l, const Paket& r) { 
                if (richtung) return l.sprache < r.sprache; 
                else return l.sprache > r.sprache;
            });
        }
        ImGui::TableNextColumn();
        if (ImGui::Selectable(TEXT_BM_JAHR, false, ImGuiSelectableFlags_DontClosePopups)) {
            static bool richtung = false;
            richtung = !richtung;
            std::sort(pakete.begin(), pakete.end(), [](const Paket& l, const Paket& r) { 
                if (richtung) return l.jahr < r.jahr; 
                else return l.jahr > r.jahr;
            });
        }
        ImGui::TableNextColumn();
        if (ImGui::Selectable(TEXT_BM_TITEL, false, ImGuiSelectableFlags_DontClosePopups)) {
            static bool richtung = false;
            richtung = !richtung;
            std::sort(pakete.begin(), pakete.end(), [](const Paket& l, const Paket& r) { 
                if (richtung) return l.titel < r.titel; 
                else return l.titel > r.titel; 
            });
        }
        ImGui::TableNextColumn();
        ImGui::TextUnformatted(TEXT_BM_VERWALTEN);
        std::string aenderungen_info(TEXT_BM_TT_AENDERUNGEN_INFO);
        Sonstiges::textwrap(aenderungen_info, 40);
        UI::tooltip(aenderungen_info.c_str());
        
        // Suche / Filter
        pakete_gefiltert.clear();
        for (auto& p : pakete) {
            std::string p_jahr_str = std::to_string(p.jahr);
            if (p.titel.find(suche) == std::string::npos && 
                p.sprache.find(suche) == std::string::npos &&
                p_jahr_str.find(suche) == std::string::npos) continue;
            pakete_gefiltert.push_back(&p);
        }

        // Tabelleninhalt
        for (unsigned i = seite * p_pro_seite; i < pakete_gefiltert.size() && i < seite * p_pro_seite + p_pro_seite; ++i) {
            Paket& p = *pakete_gefiltert[i];
            if (p.download || p.status != Paket::Status::NONE) continue; // Pakete, die gerade heruntergeladen werden separat anzeigen
            ImGui::TableNextRow();

            // Installiert?
            ImGui::TableNextColumn();
            UI::push_icons();
            ImGui::TextUnformatted(p.installiert ? "\uF046" : "\uF096");
            ImGui::PopFont();

            // Sprache
            ImGui::TableNextColumn();
            ImGui::TextUnformatted(p.sprache.c_str());

            // Jahr
            ImGui::TableNextColumn();
            if (p.jahr > 0) ImGui::Text("%d", p.jahr);

            // Titel
            ImGui::TableNextColumn();
            ImGui::TextUnformatted(p.titel.c_str());

            // Verwalten
            ImGui::TableNextColumn();
            if (p.installiert) {
                UI::push_icons();
                std::string id("\uF147");
                id.append("##delbtn");
                id.append(p.dateipfad);
                if (ImGui::Button(id.c_str())) { // Löschen
                    std::remove(p.dateipfad.c_str());
                    p.check_installiert();
                }
                ImGui::PopFont();
                if (ImGui::IsItemHovered()) ImGui::SetTooltip(TEXT_BM_TT_DEINSTALLIEREN);
            }
            else { // Hinzufügen
                UI::push_icons();
                std::string id("\uF019");
                id.append("##dlbtn");
                id.append(p.dateipfad);
                if (ImGui::Button(id.c_str())) {
                    p.download.reset(new Netz());
                    p.download->runterladen(p.url);
                    p.status = Paket::Status::DOWNLOADING;
                }
                ImGui::PopFont();
                if (ImGui::IsItemHovered()) ImGui::SetTooltip(TEXT_BM_TT_INSALLIEREN);
            }
        }
        ImGui::EndTable();
    }
    ImGui::PopFont();
    ImGui::EndPopup();
}

void Bibelmanager::pruef_downloads () {
    for (auto& p : pakete) {
        if (p.download && !p.download->is_in_arbeit() && p.download->get_fortschritt() >= 0.999f && !p.download->get_ergebnis().empty()) {
            std::filesystem::create_directory("data/buecher/" + p.sprache);
            if (std::ofstream out(p.dateipfad); out.good()) { // TODO ordner anlegen
                out << p.download->get_ergebnis();
                std::cout << "Download abgeschlossen: " << p.dateipfad << '\n';
                p.check_installiert();
                p.status = p.installiert ? Paket::Status::DOWNLOAD_SUCCESS : Paket::Status::DOWNLOAD_FAILURE;
            } else {
                std::cerr << p.dateipfad << " konnte nicht geschrieben werden.\n";
                p.status = Paket::Status::DOWNLOAD_FAILURE;
            }
            p.download.reset(nullptr);
        }
    }
}
