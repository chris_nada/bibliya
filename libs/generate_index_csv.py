import os.path, glob, bs4

class Paket:
    ausgabedatei = "pakete"
    delim = "$"
    jahr = ""
    def __init__(self, titel: str, description: str, dateiname: str, download_url: str, sprache: str):
        self.titel = titel
        self.description = description
        self.dateiname = dateiname
        self.download_url = download_url
        self.sprache = sprache
    def rate_jahr(self, text: str):
        if len(self.jahr) > 0:
            return
        try:
            for s in text.split(" "):
                if len(s) == 4 and s.isdigit():
                    self.jahr = s
                    print("Jahr gefunden:", s)
                    return
                elif len(s) == 6 and s[0] == "(" and s[5] == ")":
                    s = s.replace("(","").replace(")", "")
                    if s.isdigit():
                        self.jahr = s
                        return
        except:
            return
    def eintragen(self):
        self.rate_jahr(self.titel)
        self.rate_jahr(self.description)
        #sprache$dateipfad$jahr$name$url$desc
        with open(self.ausgabedatei, "a") as f:
            f.write(self.sprache)
            f.write(self.delim)
            f.write("data/buecher/" + self.sprache + "/" + self.dateiname) #dateipfad
            f.write(self.delim)
            f.write(self.jahr) # jahr
            f.write(self.delim)
            f.write(self.titel)
            f.write(self.delim)
            f.write(self.download_url)
            f.write(self.delim)
            f.write(self.description)
            f.write("\n")

# parse osis xml Dateien von https://github.com/gratis-bible/bible
def parse_gratis_bible():
    folder = "bible-master"
    dirs = [d for d in os.listdir(folder) if os.path.isdir(os.path.join(folder, d))]
    for dir_lang in dirs:
        for xml_file in glob.glob(folder + os.path.sep + dir_lang + os.path.sep + "*.xml"):
            print("Parse", xml_file)
            inhalt = None
            with open(xml_file, "r") as f:
                inhalt = f.read()
            soup = bs4.BeautifulSoup(inhalt, "xml")
            filename = os.path.basename(xml_file)
            titel = soup.find("title")
            titel = "" if titel is None else titel.string
            description = soup.find("description")
            description = "" if description is None else description.string
            description = "" if description is None else description.replace("\n"," ").replace("$", "S")
            description = " ".join(description.split())
            # zB https://github.com/gratis-bible/bible/raw/master/af/afr3353.xml
            download_url = "https://github.com/gratis-bible/bible/raw/master/" + dir_lang + "/" + filename
            print("Titel:", titel)
            print("Beschreibung:", description)
            print("Dateiname:", filename)
            print("URL:", download_url)
            print("\n")
            p = Paket(titel=titel, description=description, dateiname=filename, download_url=download_url, sprache=dir_lang)
            p.eintragen()
            
parse_gratis_bible()

