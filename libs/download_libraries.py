import shutil
import requests
import os
import zipfile

# Automatisch Library hinzufügen
def lib_download(name, version, link):
    print("checking", os.path.abspath(name))
    if os.path.exists(name):
        return
    print("downloading", name, version, "...")
    # wget --no-check-certificate --output-document="$1".zip "$3" # Herunterladen
    name_zip = name + ".zip"
    r = requests.get(link, verify=False)
    with open(name_zip, "wb") as f:
        f.write(r.content)
    # unzip "$1".zip                       # Entpacken
    with zipfile.ZipFile(name_zip, 'r') as temp_zip:
        temp_zip.extractall()
    # rm "$1".zip                          # zip löschen
    os.remove(name_zip)
    # mv "$1"-"$2" "$1"                    # Version aus Ordnernamen entfernen
    os.rename(name + "-" + version, name)


#           | Library, Version, Download-Link
lib_download("cereal", "1.3.2", "https://github.com/USCiLab/cereal/archive/refs/tags/v1.3.2.zip")
lib_download("imgui", "1.89.9", "https://github.com/ocornut/imgui/archive/refs/tags/v1.89.9.zip")
shutil.copy("imgui-sfml/imconfig-SFML.h", "imgui/imconfig.h")
