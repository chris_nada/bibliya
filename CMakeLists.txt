cmake_minimum_required(VERSION 3.9)
project(bibliya)

set(CMAKE_CXX_STANDARD 17)

# Global
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=core2")
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fuse-ld=lld")

# Integrierte Bibliotheken (im Ordner /libs/)   # Version   | Datum als aktuell | Beschreibung
include_directories(SYSTEM libs/imgui)          # 1.78          29 09 2020          GUI
include_directories(SYSTEM libs/imgui-sfml)     # 5ab660a       29 09 2020          SFML Schnittstelle für ImGui
include_directories(SYSTEM libs/implot)         # da5b4ab       29 09 2020          Plot-Addons für ImGui

# Build Config
if (CMAKE_BUILD_TYPE STREQUAL "Release")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2 -fopenmp") # Kompileroptimierungen #-g
    if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -s")
    endif()
    add_subdirectory(libs)
else () # Debug build
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -fopenmp")
    add_subdirectory(libs)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror")
endif ()

# Gemeinsame Bibliotheken
find_package(SFML 2 REQUIRED graphics window system) # audio
find_package(CURL REQUIRED)
include_directories(${CURL_INCLUDE_DIR})
include_directories(libs/cereal/include)

# Plattformabhängige Bibliotheken
set(LIBS bibliotheken sfml-system sfml-window sfml-graphics ${CURL_LIBRARIES})

### Debian 10
if (UNIX)
    set(LIBS ${LIBS} OpenGL GL pthread stdc++fs)
### Windows mit MSYS2
elseif (WIN32)
    find_package(CEREAL REQUIRED)
    find_package(OpenGL REQUIRED)
    set(LIBS ${LIBS} ${OPENGL_LIBRARIES} stdc++fs)
    include_directories(SYSTEM ${CEREAL_INCLUDE_DIRS})
    file(COPY "${CMAKE_CXX_COMPILER}/../libwinpthread-1.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libstdc++-6.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libgcc_s_seh-1.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libcurl-4.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libsfml-graphics-2.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libsfml-window-2.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libsfml-system-2.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libbrotlidec.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libidn2-0.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libnghttp2-14.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libssh2-1.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libpsl-5.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libfreetype-6.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../zlib1.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libzstd.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libbrotlicommon.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libbz2-1.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libharfbuzz-0.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libpng16-16.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libiconv-2.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libintl-8.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libunistring-2.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libglib-2.0-0.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libgraphite2.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
    file(COPY "${CMAKE_CXX_COMPILER}/../libpcre2-8-0.dll" DESTINATION "${CMAKE_CURRENT_LIST_DIR}/build" )
endif ()

# Build
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/build/)
file(GLOB_RECURSE QUELLTEXT src/*.hpp src/*cpp)

add_executable(bibliya_de ${QUELLTEXT})
target_link_libraries (bibliya_de ${LIBS})
target_compile_definitions(bibliya_de PRIVATE SPRACHE_DEUTSCH)

add_executable(bibliya_ua ${QUELLTEXT})
target_link_libraries (bibliya_ua ${LIBS})
target_compile_definitions(bibliya_ua PRIVATE SPRACHE_UKRAINISCH)

add_executable(bibliya_en ${QUELLTEXT})
target_link_libraries (bibliya_en ${LIBS})
target_compile_definitions(bibliya_en PRIVATE SPRACHE_ENGLISCH)
